# Required

- [x] Added D-Pad
- [x] Added Bealth Bar
- [x] Added Score
- [x] Added Joystick

# Additional

- [x] Added Sprint feature when space is pressed
- When its pressed, Speed increases and Animation gets faster
- Press Space again to disable it. There is text that tells you if its on or off
- [x] Added nice boxes around controls
- [x] Joystick zone is only in one area




